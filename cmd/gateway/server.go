package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strings"

	authpb "api_gateway/auth"
	mmpb "api_gateway/money_movement"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var (
	authClient authpb.AuthServiceClient
	mmClient   mmpb.MoneyMevementServiceClient
)

func main() {
	authConn, err := grpc.Dial("auth:9000", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := authConn.Close(); err != nil {
			log.Println(err)
		}
	}()

	authClient = authpb.NewAuthServiceClient(authConn)

	mmConn, err := grpc.Dial("money-movement:7000", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := mmConn.Close(); err != nil {
			log.Println(err)
		}
	}()

	mmClient = mmpb.NewMoneyMevementServiceClient(mmConn)

	http.HandleFunc("/login", login)
	http.HandleFunc("/customer/payment/authorize", customerPaymentAuthorize)
	http.HandleFunc("/customer/payment/capture", customerPaymentCapture)

	fmt.Println("listening on port 8080")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}

func login(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	userName, password, ok := r.BasicAuth()
	if !ok {
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}
	token, err := authClient.GetToken(ctx, &authpb.Credentials{
		UserName: userName,
		Password: password,
	})
	if err != nil {
		if _, writeErr := w.Write([]byte(err.Error())); writeErr != nil {
			log.Println(writeErr)
		}
		return
	}
	if _, err := w.Write([]byte(token.Jwt)); err != nil {
		log.Println(err)
	}
}

func customerPaymentAuthorize(w http.ResponseWriter, r *http.Request) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		log.Println("authHeader is empty")
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}
	if !strings.HasPrefix(authHeader, "Bearer ") {
		log.Println("strings.HasPrefix error")
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}
	token := strings.TrimPrefix(authHeader, "Bearer ")
	log.Println("token is: ", token)
	ctx := context.Background()
	if _, err := authClient.ValidateToken(ctx, &authpb.Token{Jwt: token}); err != nil {
		if _, writeErr := w.Write([]byte(err.Error())); writeErr != nil {
			log.Println("w.Write([]byte) error")
		}
		return
	}
	log.Println("token is validated")

	type authorizePayload struct {
		CustomerWalletUserID string `json:"customer_wallet_user_id"`
		MerchantWalletUserID string `json:"merchant_wallet_user_id"`
		Currency             string `json:"currency"`
		Cents                int64  `json:"cents"`
	}

	var payload authorizePayload

	bytes, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("io.ReadAll error")
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	log.Println("string(bytes): ", string(bytes))

	if err := json.Unmarshal(bytes, &payload); err != nil {
		log.Println("json.Unmarshal error")
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	log.Println("payload", payload)

	responseAuthorize, err := mmClient.Authorize(ctx, &mmpb.AuthorizePayload{
		CustomerWalletUserID: payload.CustomerWalletUserID,
		MerchantWalletUserID: payload.MerchantWalletUserID,
		Cents:                payload.Cents,
		Currency:             payload.Currency,
	})
	if err != nil {
		log.Println("mmClient.Authorize error", err.Error())
		if _, writeErr := w.Write([]byte(err.Error())); writeErr != nil {
			log.Println("w.Write error")
		}
		return
	}

	type response struct {
		Pid string `json:"pid"`
	}
	resp := response{
		Pid: responseAuthorize.Pid,
	}
	responseJSON, err := json.Marshal(resp)
	if err != nil {
		log.Println("json.Marshal responseJSON error")
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	_, writeErr := w.Write(responseJSON)
	if writeErr != nil {
		log.Println("w.Write responseJSON error")
		log.Println(writeErr)
		return
	}
}

func customerPaymentCapture(w http.ResponseWriter, r *http.Request) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}
	if !strings.HasPrefix(authHeader, "Bearer ") {
		http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}
	token := strings.TrimPrefix(authHeader, "Bearer ")

	ctx := context.Background()
	if _, err := authClient.ValidateToken(ctx, &authpb.Token{Jwt: token}); err != nil {
		if _, writeErr := w.Write([]byte(err.Error())); writeErr != nil {
			log.Println(writeErr)
		}
		return
	}

	type capturePayload struct {
		Pid string `json:"pid"`
	}

	var payload capturePayload

	bytes, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if err := json.Unmarshal(bytes, &payload); err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	if _, err := mmClient.Capture(ctx, &mmpb.CapturePayLoad{Pid: payload.Pid}); err != nil {
		if _, writeErr := w.Write([]byte(err.Error())); writeErr != nil {
			log.Println(writeErr)
		}
		return
	}

	w.WriteHeader(http.StatusOK)
}
